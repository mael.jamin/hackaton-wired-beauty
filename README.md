# Hackathon ESGI - wired beauty
This project is built in using the current LTS version of symfony (5.4.6)
## Services

- Postgres
- Nginx
- PHP7.4-FPM

## Installation
1. Clone the repository, build services and Create/Start containers:
```sh
$ git clone https://gitlab.com/mael.jamin/hackaton-wired-beauty.git
$ cd hackaton-wired-beauty
$ make build
$ make start
```

2. Install dependencies
```sh
$ make install
$ make node_modules
$ make ssh
$ yarn encore dev
```
3. Visit http://127.0.0.1:8080/

## Folders structure
```text
hackaton-wired-beauty/
├─ .docker/
│ ├─ nginx/
│ │   ├─ default.conf
│ │   └─ Dockerfile
│ └─ php-fpm/
│     └─ Dockerfile
│ ...
│ ├─ .env
│ └─ docker-compose.yml
```

## Commands
```sh
# Docker
$ make build
$ make start
$ make ssh
# Composer
$ make install
$ make update
# Yarn
$ make node_modules
$ make watch
# Php coding standards
$ make cs
$ make csfix
