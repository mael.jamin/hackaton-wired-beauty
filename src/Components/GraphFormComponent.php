<?php

// src/Components/AlertComponent.php
namespace App\Components;

use App\Entity\DatasetGraph;
use App\Entity\FileImport;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('GraphForm')]
class GraphFormComponent
{
    public DatasetGraph $dataset;
    public FileImport $fileImport;
    public array $graphTypes = [
        "bar" => "Bar",
        "line" => "Line",
        "radar" => "Radar",
        "polarArea" => "Polar Area",
        "pie" => "Pie",
        "doughnut" => "Doughnut"
    ];
}
