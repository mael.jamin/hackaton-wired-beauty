<?php

// src/Components/AlertComponent.php
namespace App\Components;

use App\Entity\DatasetGraph;
use App\Entity\FileImport;
use App\Entity\Study;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('StudyReport')]
class StudyReport
{
    public string $title;
    public string $logo;
    //public Study $study;
}
