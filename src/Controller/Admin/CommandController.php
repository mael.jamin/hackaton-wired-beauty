<?php

namespace App\Controller\Admin;

use App\Entity\Command;
use App\Repository\CommandRepository;
use App\Repository\StudyRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


/**
 * @Route("/command")
 */

class CommandController extends AbstractController
{
    /**
     * @Route("/", name="command_index")
     */
    public function index(CommandRepository $commandRepository,Security $security): Response
    {
        $list_command = $commandRepository->findBy(array('is_active' => false,"is_check"=>false));
        return $this->render('admin/command/index.html.twig', [
            'list_command' => $list_command
        ]);
    }

    /**
     * @Route("/{id}/active", name="active_command")
     */
    public function active_command(Command $command,CommandRepository $commandRepository,MailerService $mailerService,UserRepository $userRepository,StudyRepository $studyRepository): Response
    {
        $command->setIsActive(true);
        $command->setIsCheck(true);
        $commandRepository->add($command);
        $user = $command->getIdUser();
        $product = $command->getIdStudy();
        $mailerService->notifyConfirmDevis($user,$product);
        return $this->redirectToRoute('admin_command_index', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/{id}/deactivate", name="refuse_command")
     */
    public function refuse_command(Command $command,CommandRepository $commandRepository,MailerService $mailerService,UserRepository $userRepository,StudyRepository $studyRepository): Response
    {
        $command->setIsActive(false);
        $command->setIsCheck(true);
        $commandRepository->add($command);
        $user = $command->getIdUser();
        $product = $command->getIdStudy();
        $mailerService->notifyRefuseDevis($user,$product);
        return $this->redirectToRoute('admin_command_index', [], Response::HTTP_SEE_OTHER);
    }
}
