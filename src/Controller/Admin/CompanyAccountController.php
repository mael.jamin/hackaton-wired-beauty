<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Form\CreateCompanyAccountType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/account")
 */
class CompanyAccountController extends AbstractController
{
    /**
     * @Route("/create", name="account_create")
     */
    public function createAccount(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher, MailerInterface $mailer): Response
    {
        $form = $this->createForm(CreateCompanyAccountType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $userRole = $request->request->get('create_company_account')['accountType'];

            $user = new User();

            $user->setEmail($data->getEmail());
            $user->setPassword($passwordHasher->hashPassword($user, $data->getPlainPassword()));
            $user->setRoles([$userRole]);

            $entityManager->persist($user);
            $entityManager->flush();

            $message = (new TemplatedEmail())
                ->from('noreplay@wiredbeauty.com')
                ->to($user->getEmail())
                ->subject('Welcome to wired beauty !')
                ->htmlTemplate('email/welcome.html.twig')
                ->context([
                    'user_email' => $user->getEmail(),
                    'user_password' => $data->getPlainPassword()
                ]);
            $mailer->send($message);

            $this->addFlash('success', 'Account created successfully !');

            return $this->redirectToRoute('admin_account_listing');
        }

        return $this->render('admin/account/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/list", name="account_listing")
     */
    public function listingAccounts(UserRepository $userRepository)
    {
        $users = $userRepository->createQueryBuilder('u')
            ->where('u.email != :email')
            ->setParameter('email', $this->getUser()->getEmail())
            ->getQuery()->getResult();

        return $this->render('admin/account/list.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_account", methods={"DELETE"})
     */
    public function __invoke(User $user, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        try {
            $entityManager->remove($user);
            $entityManager->flush();
        } catch (\Exception $exception) {
            $logger->error($exception->getMessage());
        }

        return $this->redirectToRoute('admin_account_listing');
    }
}
