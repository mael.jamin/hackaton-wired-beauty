<?php

namespace App\Controller\Admin;

use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("", name="dashboard",methods={"GET"})
     */
    public function __invoke(ArticleRepository $articleRepository, UserRepository $userRepository): Response
    {
        $articlesNbr = $articleRepository->count([]);
        $usersNbr = $userRepository->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.email != :email')
            ->setParameter('email', 'admin@wb.com')
            ->getQuery()->getSingleScalarResult();

        return $this->render('admin/dashboard.html.twig', [
            'articles_nbr' => $articlesNbr,
            'users_nbr' => $usersNbr
        ]);
    }
}
