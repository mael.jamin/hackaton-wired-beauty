<?php

namespace App\Controller\Admin;
use App\Repository\StudyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/store")
 */
class StoreController extends AbstractController
{
    /**
     * @Route("", name="store_dashboard",methods={"GET"})
     */
    public function index(StudyRepository $studyRepository): Response
    {
        $list_studies_actor = ["Janvier"] ;
        return $this->render('admin/store.html.twig',[
            "studies" => $studyRepository->findAll(),
            "list_studies_actor" => $list_studies_actor
        ]);
    }

    /**
     * @Route("/addStudy", name="add_study",methods={"GET"})
     */
    public function addStudy(StudyRepository $studyRepository): Response
    {

        return $this->render('admin/store.html.twig',[
            "studies" => $studyRepository->findAll()
        ]);
    }
}