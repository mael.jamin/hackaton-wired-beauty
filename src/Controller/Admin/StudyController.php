<?php

namespace App\Controller\Admin;

use App\Entity\Study;
use App\Form\StudyType;
use App\Repository\StudyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/study")
 */
class StudyController extends AbstractController
{
    /**
     * @Route("/", name="study_index", methods={"GET"})
     */
    public function index(StudyRepository $studyRepository): Response
    {
        return $this->render('admin/study/index.html.twig', [
            'studies' => $studyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="study_new", methods={"GET", "POST"})
     */
    public function new(Request $request, StudyRepository $studyRepository): Response
    {
        $study = new Study();
        $form = $this->createForm(StudyType::class, $study);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $study->setCreatedAt(new DateTimeImmutable());
            $studyRepository->add($study);
            $session = $this->get('session');
            $session->set('studyid', $study->getId());

            return $this->redirectToRoute('admin_import_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/study/new.html.twig', [
            'study' => $study,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="study_show", methods={"GET"})
     */
    public function show(Study $study): Response
    {
        return $this->render('admin/study/show.html.twig', [
            'study' => $study,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="study_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Study $study, StudyRepository $studyRepository): Response
    {
        $form = $this->createForm(StudyType::class, $study);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $studyRepository->add($study);
            return $this->redirectToRoute('admin_study_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/study/edit.html.twig', [
            'study' => $study,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="study_delete", methods={"POST"})
     */
    public function delete(Request $request, Study $study, StudyRepository $studyRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$study->getId(), $request->request->get('_token'))) {
            $studyRepository->remove($study);
        }

        return $this->redirectToRoute('admin_study_index', [], Response::HTTP_SEE_OTHER);
    }
}
