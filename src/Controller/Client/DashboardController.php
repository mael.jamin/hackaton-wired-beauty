<?php

namespace App\Controller\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractController
{
    /**
     * @Route("", name="dashboard",methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $request->getSession()->set("list_shop_actor",[]);
        return $this->render('client/dashboard.html.twig');
    }
}