<?php

namespace App\Controller\Client;

use App\Entity\Command;
use App\Entity\Study;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\StudyRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/recape")
 */
class RecapeController extends AbstractController
{
    /**
     * @Route("", name="recape_dashboard",methods={"GET"})
     */
    public function index(Request $request,StudyRepository $studyRepository): Response
    {
        $session = $request->getSession();
        $list_id_study = $session->get("list_shop_actor");
        $list_shop_actor = [];
        foreach ($list_id_study as $study){
            $list_shop_actor = [...$list_shop_actor,$studyRepository->find($study)];
        }

        return $this->render('client/recape.html.twig',[
            "list_study" => $list_shop_actor
        ]);
    }

    /**
     * @Route("/{id}/delete", name="recapte_remove_study_in_shop", methods={"GET"})
     */
    public function removeStudy(Request $request, Study $study): Response
    {
        $session = $request->getSession();
        $list_shop_actor = $session->get("list_shop_actor");
        if (($key = array_search($study->getId(), $list_shop_actor)) !== false) {
            unset($list_shop_actor[$key]);
            $session->set("list_shop_actor",$list_shop_actor);
        }
        dump($list_shop_actor);
        return $this->redirectToRoute('client_recape_dashboard', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/buy", name="recapte_buy", methods={"GET"})
     */
    public function confirmShop(Request $request,EntityManagerInterface $entityManager,StudyRepository $studyRepository,MailerService $mailerService,Security $security): Response
    {
        $session = $request->getSession();
        $user = $security->getUser();
        $list_shop_actor = $session->get("list_shop_actor");
        $list_shop_entity = [];
        foreach ($list_shop_actor as $product){
            $entity_product = $studyRepository->find($product);
            $list_shop_entity = [...$list_shop_entity,$entity_product];
            $command = new Command();
            $command->setIdStudy($entity_product);
            $command->setIdUser($user);
            $command->setIsActive(false);
            $command->setIsCheck(false);
            $entityManager->persist($command);
        }
        $entityManager->flush();
        $mailerService->notifyBuyStudy($user,$list_shop_entity);

        $session->set("list_shop_actor",[]);
        return $this->redirectToRoute('client_recape_dashboard', [], Response::HTTP_SEE_OTHER);
    }
}