<?php

namespace App\Controller\Client;
use App\Entity\Study;
use App\Repository\CommandRepository;
use App\Repository\StudyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/store")
 */
class StoreController extends AbstractController
{
    /**
     * @Route("", name="store_dashboard",methods={"GET"})
     */
    public function index(Request $request,StudyRepository $studyRepository,Security $security,CommandRepository $commandRepository): Response
    {
        $session = $request->getSession();
        $user = $security->getUser();
        $list_pre_exist_command = $commandRepository->findBy(array("is_active"=>true,"id_user"=>$user->getId()));
        $list_pre_exist_report = [];
        foreach ($list_pre_exist_command as $command){
            $id_study = $command->getIdStudy()->getId();
            $list_pre_exist_report = [...$list_pre_exist_report,$id_study];
        }
        $list_shop_actor = $session->get("list_shop_actor");
        return $this->render('client/store.html.twig',[
            "studies" => $studyRepository->findAll(),
            "list_shop_actor" => $list_shop_actor,
            "list_pre_exist_report" => $list_pre_exist_report
        ]);
    }

    /**
     * @Route("/{id}/addStudy", name="store_add_study",methods={"GET"})
     */
    public function addStudy(Request $request,Study $study)
    {
        $session = $request->getSession();
        $list_shop_actor = $session->get("list_shop_actor");
        dump($list_shop_actor);

        if (in_array($study->getId(), $list_shop_actor)) {
            $this->addFlash("error", "L'etude na pas pu étre ajoutée");
        }
        else{
            $request->getSession()->set("list_shop_actor",[...$list_shop_actor,$study->getId()]);
            $this->addFlash(
                'success',
                "L'etude a étais ajouter au panier "
            );
        }
        return $this->redirectToRoute('client_store_dashboard', [], Response::HTTP_SEE_OTHER);
    }
}