<?php

namespace App\Controller\Client;

use App\Entity\Study;
use App\Form\StudyType;
use App\Repository\StudyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/study")
 */
class StudyController extends AbstractController
{
    /**
     * @Route("/", name="study_index", methods={"GET"})
     */
    public function index(StudyRepository $studyRepository): Response
    {
        return $this->render('client/study/index.html.twig', [
            'studies' => $studyRepository->findAll(),
        ]);
    }

}
