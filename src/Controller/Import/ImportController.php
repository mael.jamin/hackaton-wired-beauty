<?php

namespace App\Controller\Import;

use App\Entity\DatasetGraph;
use App\Entity\FileImport;
use App\Entity\Study;
use App\Entity\Report;
use App\Form\ImportType;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\DocBlock\Tags\Method;


use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Repository\StudyRepository;
use App\Repository\ReportRepository;

class ImportController extends AbstractController
{
    /**
     * @Route("admin/import/index", name="admin_import_index")
     */
    public function index(Request $request, SluggerInterface $slugger, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $fileImport = new FileImport();

        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('session')->set('reportName', $request->request->get('import')['reportName']);
            $datasetFile = $form->get('dataset')->getData();
            $originalName = pathinfo($datasetFile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalName);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$datasetFile->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $datasetFile->move(
                    $this->getParameter('files_directory'),
                    $newFilename
                );
            } catch (FileException $e) {
                dd($e->getMessage());
            }

            try {
                $finalFileName = $this->getParameter('files_directory')."/".$newFilename;
            }catch (ServiceNotFoundException $e){
                die("Impossible de trouver le répertoire principal");
            }

            try {
                $headers = $this->getExcelHeader($this->getExcelWorksheet($finalFileName, new HeaderFilter()));
            }catch (\PhpOffice\PhpSpreadsheet\Exception | \PhpOffice\PhpSpreadsheet\Reader\Exception $ex){
                die("Lecture de l'entete du fichier XSLX impossible");
            }

            $fileImport
                ->setFilename($newFilename)
                ->setHeaders($headers)
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable());
            $entityManager->persist($fileImport);
            $entityManager->flush();

            return $this->redirectToRoute('admin_import_view',[
               "fileImport" => $fileImport->getId()
            ]);
        }

        return $this->renderForm('import/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function getExcelContent(Worksheet $sheet): array
    {
        $headers = $this->getExcelHeader($sheet);

        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
        $content = array();
        for ($ligne = 2; $ligne < $highestRow; $ligne++){
            for ($col = 0; $col < $highestColumnIndex; $col++) {
                $value = $sheet->getCellByColumnAndRow($col, $ligne)->getValue();
                if (!is_null($value))
                    $content[$ligne][$headers[$col]] = $value;
            }
        }
        return $content;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function getExcelHeader(Worksheet $sheet): array{
        $highestColumn = $sheet->getHighestColumn();
        $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
        $header = array();
        for($i=0; $i<$highestColumnIndex; $i++){
            $value = $sheet->getCellByColumnAndRow($i, 1)->getValue();
            if (!is_null($value))
                $header[$i] = $value;
        }
        return $header;
    }

    private function getExcelWorksheet($inputFileName, IReadFilter $filter = null){
        $inputFileType = IOFactory::identify($inputFileName);
        /**  Create a new Reader of the type that has been identified  **/
        $reader = IOFactory::createReader($inputFileType);
        /** On ne lit que l'entete **/
        if ($filter)
            $reader->setReadFilter($filter);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);
        $activesheet = $spreadsheet->getActiveSheet();
        return $activesheet;
    }

    /**
     * @Route("/view/add_graph", name="admin_add_graph", methods={"GET"})
     */
    public function addGraph(Request $request, ManagerRegistry $doctrine, StudyRepository $studyRepository, ReportRepository $reportRepository){
        $entityManager = $doctrine->getManager();

        $studyId = $this->get('session')->get('studyid');

        if (!$studyId) {
            throw new \Exception('You need to create new study !');
        }

        $study = $studyRepository->find($studyId);
        $reportOld = $reportRepository->findOneBy(['study' => $study]);
        if ($reportOld) {
            $report = $reportOld;
        } else {
            $report = new Report();
            $report->setName($this->get('session')->get('reportName'));
            $report->setStudy($study);
            $entityManager->persist($report);
            $entityManager->flush();
        }

        $datasetGraph = new DatasetGraph();
        $id_import = $request->get('import', null);
        if ($id_import){
            $fileImport = $entityManager->find(FileImport::class, $id_import);
            $datasetGraph
                ->setFile($fileImport)
                ->setAbs($request->get('abs'))
                ->setOrd($request->get('ord'))
                ->setType($request->get('type'))
                ->setName($request->get('name'))
                ->setReport($reportRepository->find($report->getId()))
            ;
            $entityManager->persist($datasetGraph);
            $entityManager->flush();
            return $this->redirectToRoute('admin_import_view', ["fileImport" => $fileImport->getId()]);
        }
        return $this->redirectToRoute('admin_import_index');
    }

    /**
     * @Route("/view/edit_graph", name="admin_edit_graph", methods={"GET"})
     */
    public function edit(Request $request, ManagerRegistry $doctrine){
        $entityManager = $doctrine->getManager();
        $datasetGraph = $entityManager->find(DatasetGraph::class, $request->get('graph'));
        $fileImport = $datasetGraph->getFile()->getId();
        if ($request->get('action') === 'edit'){ //EDIT
            $datasetGraph
                ->setAbs($request->get('abs'))
                ->setOrd($request->get('ord'))
                ->setType($request->get('type'))
                ->setName($request->get('name'))
            ;
        }else{  //DELETE
            $entityManager->remove($datasetGraph);
        }
        $entityManager->flush();
        return $this->redirectToRoute('admin_import_view',["fileImport" => $fileImport]);
    }

    /**
     * @Route("/view/{fileImport}", name="admin_import_view", methods={"GET"})
     */
    public function view(FileImport $fileImport){
        $datasets = $fileImport->getDatasetGraphs();
        $data = array();
        if ($datasets->count() > 0){
            $finalFileName = $this->getParameter('files_directory')."/".$fileImport->getFilename();
            $worksheet = $this->getExcelWorksheet($finalFileName);
            try{
                $content = $this->getExcelContent($worksheet);
            }catch (\PhpOffice\PhpSpreadsheet\Exception | \PhpOffice\PhpSpreadsheet\Reader\Exception $ex){
                die("Impossible de lire le fichier Excel");
            }

            foreach ($datasets as $dataset){
                $id = $dataset->getId();
                $abs = $dataset->getAbs();
                $ord = $dataset->getOrd();
                $type = $dataset->getType();
                $name = $dataset->getName();

                $ord_values = array_column($content,$ord);
                if ($abs !== "iteration"){
                    $abs_values = array_column($content,$abs);
                }else{
                    $abs_values = range(1,count($ord_values));
                }
                $final_values = array_combine($abs_values,$ord_values);
                $data[$id]["data"]["datasets"]["label"] = $name;
                $data[$id]["data"]["datasets"]["data"] = $final_values;
                $data[$id]["type"] = $type;
            }
        }

        return $this->render('import/view.html.twig',[
            "fileImport" => $fileImport,
            "datasets" => $datasets,
            "data_content" => $data
        ]);
    }
}


class HeaderFilter implements IReadFilter
{
    public function readCell($columnAddress, $row, $worksheetName = '') {
        //  Read rows 1 to 7 and columns A to E only
        if ($row == 1) {
            return true;
        }
        return false;
    }
}
