<?php

namespace App\Controller;

use App\Form\ResetPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/admin/change-password", name="admin_change_password")
     * @Route("/client/change-password", name="client_change_password")
     * @Route("/scientist/change-password", name="scientist_change_password")
     */
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordEncoder
    ) {
        $user = $this->getUser();

        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $request->request->get('reset_password')['oldPassword'];

            if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                $newPassword = $passwordEncoder->hashPassword($user, $user->getPlainPassword());
                $user->setPassword($newPassword);
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'Votre mot de passe à bien été changé !');

                if (in_array('ROLE_ADMIN', $user->getRoles())) {
                    $this->redirectToRoute('admin_change_password');
                }
                if (in_array('ROLE_SCIENTIST', $user->getRoles())) {
                    $this->redirectToRoute('scientist_change_password');
                }
                if (in_array('ROLE_COMPANY', $user->getRoles())) {
                    $this->redirectToRoute('client_change_password');
                }
            } else {
                $this->addFlash('danger', 'Ancien mot de passe incorrect !');
            }
        }

        return $this->render('reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
