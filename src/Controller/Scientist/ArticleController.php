<?php

namespace App\Controller\Scientist;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Stichoza\GoogleTranslate\GoogleTranslate;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/create", name="create_article")
     */
    public function addArticle(Request $request, EntityManagerInterface $entityManager)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** Upload article image */
            $imageFile = $form['imageFile']->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $this->getParameter('article_images_dir'),
                    $newFilename
                );
                $article->setImageName($newFilename);
            }
            /** end upload article image */
            $article->setPublishedBy($this->getUser());
            $article->setIsMain(true);
            $article->setMainArticle($article);
            $entityManager->persist($article);
            $entityManager->flush();
            $this->addFlash('success', 'Article created successfully !');

            return $this->redirectToRoute('scientist_articles_listing');
        }

        return $this->render('scientist/article/create.html.twig', [
            'articleForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/list", name="articles_listing")
     */
    public function ArticlesList(Request $request, ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findBy([
            'isMain' => true,
            'publishedBy' => $this->getUser()
        ]);
        return $this->render('scientist/article/list.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_article", methods={"DELETE"})
     */
    public function delete(Article $article, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        try {
            $entityManager->remove($article);
            $entityManager->flush();
        } catch (\Exception $exception) {
            $logger->error($exception->getMessage());
        }

        return $this->redirectToRoute('scientist_articles_listing');
    }

    /**
     * @Route("/edit/{id}/{lang}", name="edit_article")
     * @ParamConverter("article", options={"mapping": {"id": "id"}})
     */
    public function editArticle(Article $article,Request $request, EntityManagerInterface $entityManager, LoggerInterface $logger, ArticleRepository $articleRepository)
    {
            $languageCode = $request->attributes->get('lang');

            $articleToEdit = $articleRepository->findOneBy(['mainArticle' => $article, 'langcode' => $languageCode]);
            $form = $this->createForm(ArticleType::class, $articleToEdit);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** Upload article image */
                $imageFile = $form['imageFile']->getData();
                if ($imageFile) {
                    $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$imageFile->guessExtension();
                    $imageFile->move(
                        $this->getParameter('article_images_dir'),
                        $newFilename
                    );
                    $article->setImageName($newFilename);
                }
                /** end upload article image */

                $entityManager->persist($article);
                $entityManager->flush();
                $this->addFlash('success', 'Article updated successfully !');

                return $this->redirectToRoute('scientist_articles_listing');
            }

            return $this->render('scientist/article/edit.html.twig', [
                'articleForm' => $form->createView(),
                'lang' => $articleToEdit->getLangcode()
            ]);
    }
}
