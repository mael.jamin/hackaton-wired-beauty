<?php

namespace App\Controller\Scientist;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractController
{
    /**
     * @Route("", name="dashboard",methods={"GET"})
     */
    public function __invoke(): Response
    {
        return $this->render('scientist/dashboard.html.twig');
    }
}
