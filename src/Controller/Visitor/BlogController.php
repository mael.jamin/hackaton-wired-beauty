<?php

namespace App\Controller\Visitor;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/blog")
 */
class BlogController  extends AbstractController
{
    /**
     * @Route("", name="blog_dashboard",methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->findBy(['isMain' => true]);

        return $this->render('visitor/blog.html.twig',[
            "articles" => $articles
        ]);
    }

    /**
     * @Route("/{id}/show", name="show_article",methods={"GET"})
     */
    public function show_article(Article $article): Response
    {
        return $this->render('visitor/article.html.twig',[
            "article" => $article
        ]);
    }
}