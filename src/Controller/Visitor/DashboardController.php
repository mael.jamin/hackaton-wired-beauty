<?php

namespace App\Controller\Visitor;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard",methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('visitor/dashboard.html.twig');
    }
    /**
     * @Route("what-we-do", name="what_we_do",methods={"GET"})
     */
    public function what_we_do(): Response
    {
        return $this->render('visitor/what_we_do.html.twig');
    }
    /**
     * @Route("new-nomadic-labgrade-device", name="new_nomadic_labgrade_device",methods={"GET"})
     */
    public function new_nomadic_labgrade_device(): Response
    {
        return $this->render('visitor/nomadic.html.twig');
    }

    /**
     * @Route("who-we-are", name="who_we_are",methods={"GET"})
     */
    public function who_we_are(): Response
    {
        return $this->render('visitor/who_we_are.html.twig');
    }
    /**
     * @Route("scientific-validation", name="scientific_validation",methods={"GET"})
     */
    public function scientific_validation(): Response
    {
        return $this->render('visitor/scientific_validation.html.twig');
    }
    /**
     * @Route("studies-services", name="studies_services",methods={"GET"})
     */
    public function studies_services(): Response
    {
        return $this->render('visitor/studies_services.html.twig');
    }

    /**
     * @Route("/change_locale/{locale}", name="change_locale")
     */
    public function changeLocale($locale,Request $request)
    {
        $request->getSession()->set('_locale', $locale);
        return $this->redirect($request->headers->get('referer'));
    }





}
