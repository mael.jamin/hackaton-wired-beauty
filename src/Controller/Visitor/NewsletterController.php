<?php

namespace App\Controller\Visitor;

use App\Entity\Newsletter;
use App\Repository\NewsletterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractController
{

    /**
     * @Route("/new", name="add_newsletter", methods={"GET"})
     */
    public function new(Request $request, NewsletterRepository $newsletterRepository): Response
    {
        $newsletter = new Newsletter();
        $newsletter->setEmail($request->get("email"));
        $newsletterRepository->add($newsletter);
        return $this->redirectToRoute('visitor_dashboard', [], Response::HTTP_SEE_OTHER);

    }
}
