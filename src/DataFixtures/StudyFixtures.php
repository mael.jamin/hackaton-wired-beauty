<?php

namespace App\DataFixtures;

use App\Entity\Study;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class StudyFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 5; $i++) {
            $study = new Study();
            $study->setName('Etude- '.$i);
            $study->setDescription("Lorem Imsum");
            $study->setCreatedAt(new \DateTimeImmutable());
            $study->setPrice($i);
            $manager->persist($study);
        }
        $manager->flush();
    }

}