<?php

namespace App\Entity;

use App\Repository\CommandRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandRepository::class)
 */
class Command
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Study::class, inversedBy="commands")
     */
    private $id_study;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commands")
     */
    private $id_user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_check;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdStudy(): ?Study
    {
        return $this->id_study;
    }

    public function setIdStudy(?Study $id_study): self
    {
        $this->id_study = $id_study;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsCheck(): ?bool
    {
        return $this->is_check;
    }

    public function setIsCheck(bool $is_check): self
    {
        $this->is_check = $is_check;

        return $this;
    }
}
