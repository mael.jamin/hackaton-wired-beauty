<?php

namespace App\Entity;

use App\Repository\DatasetGraphRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DatasetGraphRepository", repositoryClass=DatasetGraphRepository::class)
 */
class DatasetGraph
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FileImport::class, inversedBy="datasetGraphs")
     * @ORM\JoinColumn (nullable=false)
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $abs;

    /**
     * @ORM\Column(type="string")
     */
    private $ord;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Report::class, inversedBy="datasetGraphs")
     */
    private $report;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?FileImport
    {
        return $this->file;
    }

    public function setFile(?FileImport $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getAbs(): ?string
    {
        return $this->abs;
    }

    public function setAbs(string $abs): self
    {
        $this->abs = $abs;

        return $this;
    }

    public function getOrd(): ?string
    {
        return $this->ord;
    }

    public function setOrd(string $ord): self
    {
        $this->ord = $ord;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReport(): ?Report
    {
        return $this->report;
    }

    public function setReport(?Report $report): self
    {
        $this->report = $report;

        return $this;
    }
}
