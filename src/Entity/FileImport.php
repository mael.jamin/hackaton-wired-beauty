<?php

namespace App\Entity;

use App\Repository\FileImportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileImportRepository", repositoryClass=FileImportRepository::class)
 */
class FileImport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=DatasetGraph::class, mappedBy="file", orphanRemoval=true)
     */
    private $datasetGraphs;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $headers = [];

    public function __construct()
    {
        $this->datasetGraphs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, DatasetGraph>
     */
    public function getDatasetGraphs(): Collection
    {
        return $this->datasetGraphs;
    }

    public function addDatasetGraph(DatasetGraph $datasetGraph): self
    {
        if (!$this->datasetGraphs->contains($datasetGraph)) {
            $this->datasetGraphs[] = $datasetGraph;
            $datasetGraph->setFile($this);
        }

        return $this;
    }

    public function removeDatasetGraph(DatasetGraph $datasetGraph): self
    {
        if ($this->datasetGraphs->removeElement($datasetGraph)) {
            // set the owning side to null (unless already changed)
            if ($datasetGraph->getFile() === $this) {
                $datasetGraph->setFile(null);
            }
        }

        return $this;
    }

    public function getHeaders(): ?array
    {
        return $this->headers;
    }

    public function setHeaders(?array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }
}
