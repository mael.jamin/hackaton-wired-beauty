<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use App\Traits\BaseTrait;
use App\Traits\CreationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Report
{
    use BaseTrait;
    use CreationTrait;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Study::class, inversedBy="reports")
     */
    private $study;

    /**
     * @ORM\OneToMany(targetEntity=DatasetGraph::class, mappedBy="report")
     */
    private $datasetGraphs;

    public function __construct()
    {
        $this->datasetGraphs = new ArrayCollection();
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStudy(): ?Study
    {
        return $this->study;
    }

    public function setStudy(?Study $study): self
    {
        $this->study = $study;

        return $this;
    }

    /**
     * @return Collection<int, DatasetGraph>
     */
    public function getDatasetGraphs(): Collection
    {
        return $this->datasetGraphs;
    }

    public function addDatasetGraph(DatasetGraph $datasetGraph): self
    {
        if (!$this->datasetGraphs->contains($datasetGraph)) {
            $this->datasetGraphs[] = $datasetGraph;
            $datasetGraph->setReport($this);
        }

        return $this;
    }

    public function removeDatasetGraph(DatasetGraph $datasetGraph): self
    {
        if ($this->datasetGraphs->removeElement($datasetGraph)) {
            // set the owning side to null (unless already changed)
            if ($datasetGraph->getReport() === $this) {
                $datasetGraph->setReport(null);
            }
        }

        return $this;
    }
}
