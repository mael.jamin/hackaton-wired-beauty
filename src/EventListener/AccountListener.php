<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class AccountListener
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(User $article, LifecycleEventArgs $args)
    {
        $user = $args->getObject();
        if (!$user instanceof User) {
            return;
        }

        $message = (new TemplatedEmail())
            ->from('noreplay@wiredbeauty.com')
            ->to($user->getEmail())
            ->subject('Welcome to wired beauty !')
            ->htmlTemplate('email/welcome.html.twig')
            ->context([
                'user' => $user,
            ]);
        $this->mailer->send($message);
    }
}
