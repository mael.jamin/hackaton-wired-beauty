<?php

namespace App\EventListener;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Stichoza\GoogleTranslate\GoogleTranslate;

class ArticleListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $languages;

    public function __construct($languages, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->languages = $languages;
    }

    public function __invoke(Article $article, LifecycleEventArgs $args)
    {
        $currentArticle = $args->getObject();
        if (!$currentArticle instanceof Article || !$currentArticle->getIsMain()) {
            return;
        }

        foreach ($this->languages as $language) {
            if ($language == $currentArticle->getLangcode()) {
                continue;
            }

            $tr = new GoogleTranslate($language);
            $tr->setSource($currentArticle->getLangcode());

            $article = new Article();
            $article->setTitle($tr->translate($currentArticle->getTitle()));
            $article->setText($tr->translate($currentArticle->getText()));
            $article->setLangcode($language);
            $article->setImageName($currentArticle->getImageName());
            $article->setMainArticle($currentArticle);
            $article->setPublishedBy($currentArticle->getPublishedBy());

            $this->entityManager->persist($article);
        }

        $this->entityManager->flush();
    }
}
