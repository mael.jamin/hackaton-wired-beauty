<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title',TextType::class,[
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('text', TextareaType::class,[
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('imageFile', FileType::class, [
                'mapped' => false,
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('langcode', ChoiceType::class, [
                'choices' => [
                    'Francais' => 'fr',
                    'Anglais' => 'en',
                    'Allemand' => 'de',
                    'Chinois' => 'zh'
                ],
                'data' => 'fr',
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
