<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateCompanyAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [
                    new NotBlank(array("message" => "Veuillez fournir un email valide")),
                    new Email(array("message" => "Votre email ne semble pas valide")),
                ]
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => ['label' => false, 'attr' => [
                    'class' => 'form-control'
                ],],
                'second_options' => ['label' => false, 'attr' => [
                    'class' => 'form-control'
                ],],
                'constraints' => [
                    new Length(array("min" => 5, "minMessage" => "Le mot de passe doit comporter plus de 5 caractères")),
                ]
            ))
            ->add('companyName', TextType::class,[
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('accountType', ChoiceType::class, [
                'mapped' => false,
                'choices' => [
                    'Company' => 'ROLE_COMPANY',
                    'Scientist' => 'ROLE_SCIENTIST',
                    'Admin' => 'ROLE_ADMIN'
                ],
                'label' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
