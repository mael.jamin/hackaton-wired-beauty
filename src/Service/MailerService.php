<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\User;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    private function sendTemplateEmail($to, $subject, $template, $context): bool
    {
        $email = (new TemplatedEmail())
            ->from("wired-beauty@support.com")
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);

        $this->mailer->send($email);
        return true;
    }

    public function notifyBuyStudy(UserInterface $user, array $product): bool
    {
        $subject = "Commande";
        $template = "client/product/product.html.twig";
        $context = ['user' => $user, 'product' => $product];
        $this->sendTemplateEmail($user->getEmail(), $subject, $template, $context);
        $this->notifyClientNewCommand($user,$product);

        return true;
    }
    public function notifyConfirmDevis(User $user, $product): bool
    {
        $subject = "Confirmation du devis";
        $template = "client/product/confirm.html.twig";
        $context = ['user' => $user, 'product' => $product];
        $this->sendTemplateEmail($user->getEmail(), $subject, $template, $context);

        return true;
    }
    public function notifyRefuseDevis(User $user, $product): bool
    {
        $subject = "Refus du devis";
        $template = "client/product/refuse.html.twig";
        $context = ['user' => $user, 'product' => $product];
        $this->sendTemplateEmail($user->getEmail(), $subject, $template, $context);

        return true;
    }

    public function notifyClientNewCommand(UserInterface $user, array $product): bool
    {
        $subject = "Nouveau devis";
        $template = "client/product/devis.html.twig";
        $context = ['user' => $user, 'product' => $product];
        $this->sendTemplateEmail("wired-beauty@support.com", $subject, $template, $context);
        return true;
    }

    public function notifyUserNewArticle(Article $article, string $email){
        $subject = "Nouvel article en ligne !";
        $template = "email/newsletter.html.twig";
        $context = ['article' => $article];
        $this->sendTemplateEmail($email, $subject, $template, $context);
        return true;
    }
}
